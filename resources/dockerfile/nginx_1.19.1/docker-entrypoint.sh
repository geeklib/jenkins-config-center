#!/bin/bash
{
    while inotifywait -q -o reload.log /usr/local/nginx/conf --timefmt '%d/%m/%y/%H:%M' --format '%T %w %f %e' -e modify,delete,create,attrib; \
    do
        nginx -s reload
    done
} &
exec "$@"
